package synchCollection;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.CopyOnWriteArrayList;

public class Ch15_Demo13 {
	public static void main(String[] args) {
		Collection<User> users = new ArrayList<User>();
		// Collection<User> users = new CopyOnWriteArrayList<User>();
		users.add(new User("张三", 28));
		users.add(new User("李四", 25));
		users.add(new User("王五", 31));
		Iterator itrUsers = users.iterator();
		while (itrUsers.hasNext()) {
			User user = (User) itrUsers.next();
			if ("张三".equals(user.getName())) {
				users.remove(user);
				// itrUsers.remove();
			} else {
				System.out.println(user);
			}
		}
	}
}
