package thread;

public class RunnableThread {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// 以下方法中实现Runnable的方法，然后实现自己的run方法，肯定是执行自己的run方法
		new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("runnable " + Thread.currentThread().getName());
				}

			}
		}) {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(50);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("thread " + Thread.currentThread().getName());
				}
			}
		}.start();

	}

}
