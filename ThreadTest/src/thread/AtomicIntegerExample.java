package thread;

import java.util.concurrent.atomic.AtomicInteger;

public class AtomicIntegerExample {
	private static AtomicInteger at = new AtomicInteger(0);

	static class MyRunnable implements Runnable {

		private int myCounter;
		private int myPrevCounter;
		private int myCounterPlusFive;
		private boolean isNine;

		public void run() {
			// 以原子方式将当前值加 1,返回的是更新的值
			myCounter = at.incrementAndGet();
			System.out.println("Thread " + Thread.currentThread().getId() + "  / Counter : "
			        + myCounter);
			// 以原子方式将当前值加 1,返回的是以前的值
			myPrevCounter = at.getAndIncrement();
			System.out.println("Thread " + Thread.currentThread().getId() + " / Previous : "
			        + myPrevCounter);
			// 以原子方式将给定值与当前值相加
			myCounterPlusFive = at.addAndGet(5);
			System.out.println("Thread " + Thread.currentThread().getId() + " / plus five : "
			        + myCounterPlusFive);
			// 如果当前值 == 预期值，则以原子方式将该值设置为给定的更新值
			isNine = at.compareAndSet(9, 3);
			if (isNine) {
				System.out.println("Thread " + Thread.currentThread().getId()
				        + " / Value was equal to 9, so it was updated to " + at.intValue());
			}

		}
	}

	public static void main(String[] args) {
		Thread t1 = new Thread(new MyRunnable());
		Thread t2 = new Thread(new MyRunnable());
		t1.start();
		t2.start();
	}
}
