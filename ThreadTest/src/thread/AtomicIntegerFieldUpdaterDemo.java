package thread;

import java.util.concurrent.atomic.*;

/**
 * 
 * <p>Title:Container</p>
 * <p>Description:Volatile的意思是说：在jvm中，一个线程更新了共享变量i，另外一个线程立即去读取共享区中的i时，读到的可能不是刚才另外那个线程更新过的结果，
 * 这就类似数据库中的事务隔离级别中的read uncommited，volatile就是解决这个问题的。
 * volatie 并不能保证并发互斥性,仅仅是保证了修改的可见性,
 * 表示对valotie的变量进行修改可以被其它线程立即访问,但是无法保证多线程并发修改的安全性.但是可以用AtomicIntegerFieldUpdater类型来实现其原子更新. 样例代码</p>
 * @author	xiongfei
 * @date	2017年6月14日 下午10:01:08
 * @version 1.0
 * 
 */
class Container {
	public volatile int no = 0;
}

class Task extends Thread {
	// 使用给定字段为对象创建和返回一个更新器。
	// 需要 Class 参数来检查反射类型和一般类型是否匹配。
	private AtomicIntegerFieldUpdater<Container> updater = AtomicIntegerFieldUpdater.newUpdater(
	        Container.class, "no");
	private Container c;

	public Task(Container c) {
		this.c = c;
	}

	@Override
	public void run() {
		// 以原子方式将此更新器管理的给定对象的当前值减 1,返回以前的值
		System.out.println(Thread.currentThread().getName() + " " + updater.getAndIncrement(c));
		System.out.println(Thread.currentThread().getName() + " " + updater.getAndIncrement(c));
	}
}

public class AtomicIntegerFieldUpdaterDemo {
	public static void main(String[] args) {
		Container c = new Container();
		Task t1 = new Task(c);
		Task t2 = new Task(c);
		t1.start();
		t2.start();
	}
}
