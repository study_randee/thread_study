package thread;

public class TraditionalThread {

	public static void main(String[] args) {

		// 方式一创建线程
		// 在Thread子类覆盖的run方法中编写运行代码
		Thread thread = new Thread() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(500L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("方式一1:" + Thread.currentThread().getName());
					// 这里的this其实就是当前线程，所以和Thread.currentThread()起到了相同的效果
					System.out.println("方式一2:" + this.getName());
				}
			}
		};
		thread.start();

		// 方式二创建线程
		// 在传递给Thread对象的Runnable对象的run方法中编写代码
		Thread thread2 = new Thread(new Runnable() {
			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(500L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("方式一3:" + Thread.currentThread().getName());

				}

			}
		});
		thread2.start();

		// 这里运行的是thread部分，不是运行runnable部分
		new Thread(new Runnable() {
			public void run() {
				while (true) {
					try {
						Thread.sleep(500L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("runnable :" + Thread.currentThread().getName());

				}
			}
		}) {
			public void run() {
				while (true) {
					try {
						Thread.sleep(500L);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					System.out.println("thread :" + Thread.currentThread().getName());

				}
			}
		}.start();

	}

}
