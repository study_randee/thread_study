package thread;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

class ThreadScopeShareData {
	// 三个模块共享数据，主线程模块和AB模块
	private static int data = 0; // 准备共享的数据
	// 存放各个线程对应的数据
	private static Map<Thread, Integer> threadData = new HashMap<Thread, Integer>();

	public static void main(String[] args) { // 创建两个线程
		for (int i = 0; i < 2; i++) {
			new Thread(new Runnable() {
				public void run() { 
					// 先在当前线程中修改一下数据,给出修改信息
					// data = new Random().nextInt();
					int data = new Random().nextInt();
					System.out.println(Thread.currentThread().getName() + "将数据改为" + data);
					// 将线程信息和对应数据存储起来
					threadData.put(Thread.currentThread(), data);
					// 使用两个不同的模块操作这个数据，看结果
					new A().get();
					new B().get();
				}
			}).start();
		}
	}

	static class A {
		public void get() {
			int data = threadData.get(Thread.currentThread());
			System.out.println("A from " + Thread.currentThread().getName() + "拿到的数据" + data);
		}
	}

	static class B {
		public void get() {
			int data = threadData.get(Thread.currentThread());
			System.out.println("B from " + Thread.currentThread().getName() + "拿到的数据" + data);
		}
	}
}
