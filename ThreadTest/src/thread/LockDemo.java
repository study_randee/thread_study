package thread;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LockDemo {
	static OutPut output = new OutPut();

	public static void main(String[] args) {

		MyTherad1 mytherad1 = new MyTherad1();
		Thread thread1 = new Thread(mytherad1);

		MyTherad2 mytherad2 = new MyTherad2();
		Thread thread2 = new Thread(mytherad2);

		thread1.start();
		thread2.start();
	}

	static class OutPut {
		Lock lock = new ReentrantLock();

		public void output(String name) {
			try {
				lock.lock();
				for (int i = 0; i < name.length(); i++) {
					System.out.print(name.charAt(i));
				}
				System.out.println();
			} catch (Exception e) {
				e.printStackTrace();
			} finally {
				lock.unlock();
			}
		}
	}

	static class MyTherad1 implements Runnable {

		@Override
		public void run() {
			while (true) {
				output.output("&xiongfei");
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}

	}

	static class MyTherad2 implements Runnable {

		@Override
		public void run() {
			while (true) {
				output.output("@fengyuan");
				try {
					Thread.sleep(20);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
