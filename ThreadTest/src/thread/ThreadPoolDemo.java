package thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class ThreadPoolDemo {

	public static void main(String[] args) {
		// 创建固定大小的线程池，三个线程
		ExecutorService threadPool = Executors.newFixedThreadPool(3);

		for (int i = 1; i <= 10; i++) {
			final int taskid = i;
			threadPool.execute(new Runnable() {

				@Override
				public void run() {
					for (int j = 1; j <= 10; j++) {
						try {
							Thread.sleep(100);
							System.out.println(Thread.currentThread().getName() + "--loop of--" + j
							        + " taskid=" + taskid);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			});
		}
		System.out.println("all task is complete");

	/*	Executors.newScheduledThreadPool(3).schedule(new Runnable() {

			@Override
			public void run() {
				System.out.println("--bombing--");

			}
		}, 5, TimeUnit.SECONDS);

		// 间隔重复任务的定时方式
		Executors.newScheduledThreadPool(3).scheduleWithFixedDelay(new Runnable() {

			@Override
			public void run() {
				System.out.println("--bombing2--");

			}
		}, 1, 2, TimeUnit.SECONDS);*/

		threadPool.shutdown();

	}

}
