package timer;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * @author xiongf
 * 实现连环调用定时器，一会2秒触发，一会4秒触发
 * 交替进行
 */
public class ChainTimer {
	static int i = 0;

	public static void main(String[] args) {
		Timer timer = new Timer();
		timer.schedule(new MyTimerTask(), 2000);
		while (true) {
			try {
				System.out.println(Calendar.getInstance().get(Calendar.SECOND));
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}

class MyTimerTask extends TimerTask {
	@Override
	public void run() {
		int count = (ChainTimer.i++) % 2;
		System.out.println("--borming--");
		Timer timer = new Timer();
		timer.schedule(new MyTimerTask(), 2000 + 2000 * count);
	}
}
