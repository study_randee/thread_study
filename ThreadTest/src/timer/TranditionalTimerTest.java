package timer;

import java.util.Calendar;
import java.util.Timer;
import java.util.TimerTask;

public class TranditionalTimerTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		new Timer().schedule(new TimerTask() {// 定时执行的任务
			        @Override
			        public void run() {
				        System.out.println("timer trigger at :"
				                + Calendar.getInstance().get(Calendar.SECOND));
			        }
		        }, 1000, // 定好的延迟时间,1秒之后执行
		        3000);// 每间隔3秒再执行一次

	}
}
