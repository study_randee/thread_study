package test;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class MultiThreadSet {

	// private static BatchBean bean;

	private BatchBean bean;

	private static MultiThreadSet instance = null;

	private static Integer synInsObj = 1;

	public static MultiThreadSet getInstance() {
		if (null != instance) {
			return instance;
		}

		synchronized (synInsObj) {
			if (null != instance) {
				return instance;
			}
			instance = new MultiThreadSet();
		}
		return instance;
	}

	static {
		FileUtils.deleteQuietly(new File("D:/java/workspace/ThreadTest/threadtest.log"));
	}

	// public void init(BatchBean a_batchExecuteBean) {
	public synchronized void init(BatchBean a_batchExecuteBean) throws Exception {
		bean = a_batchExecuteBean;
		// System.out.println("我在init");
		FileUtils.writeStringToFile(new File("D:/java/workspace/ThreadTest/threadtest.log"), Thread
		        .currentThread().getName() + "【我在init】" + a_batchExecuteBean + "\r\n", true);

		// Thread.sleep(5000L);
	}

	public synchronized void task(String taskType) throws InterruptedException, IOException {
		// System.out.println(taskType + bean);
		// Thread.sleep(50L);
		FileUtils.writeStringToFile(new File("D:/java/workspace/ThreadTest/threadtest.log"), Thread
		        .currentThread().getName() + "【我在task】" + taskType + bean + "\r\n", true);
	}

	public synchronized void task2(String taskType, BatchBean a_batchExecuteBean)
	        throws InterruptedException, IOException {
		// Thread.sleep(500L);
		System.out.println(taskType + a_batchExecuteBean);
		FileUtils.writeStringToFile(new File("D:/java/workspace/ThreadTest/threadtest.log"),
		        taskType + a_batchExecuteBean + "\r\n", true);
	}

	public static void main(String[] args) {
		ThreadA threadA = new ThreadA();
		ThreadB threadB = new ThreadB();

		threadA.start();
		threadB.start();
	}
}
