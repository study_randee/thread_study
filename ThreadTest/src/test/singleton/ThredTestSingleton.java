/** 
 * Project Name:ThreadTest 
 * File Name:ThredTestSingleton.java 
 * Package Name:test.singleton 
 * Copyright (c) 2017, randeexiong@163.com All Rights Reserved. 
 */

package test.singleton;

/**
 * <p>Title:ThredTestSingleton</p>
 * <p>Description:TODO</p>
 * @author	xiongfei
 * @date	2017年6月15日 下午2:25:59
 * @version 1.0
 */

public class ThredTestSingleton {

	public static BeanParam bp1;
	public static BeanParam bp2;

	public static ThredTestSingleton singleton = new ThredTestSingleton();

	public static BeanParam bp = null;

	public ThredTestSingleton() {}

	public static ThredTestSingleton getInstance() {
		return singleton;
	}

	public void init(BeanParam bp) {
		this.bp = bp;

	}

	public void init1(BeanParam bp) {
		// this.bp = bp;
		ThredTestSingleton.bp=bp;
		// this.bp1 = this.bp;
		// ThredTestSingleton.bp1 = this.bp;
		// System.out.println(this.bp);
		System.out.println(ThredTestSingleton.bp);

	}

	public void init2(BeanParam bp) {
		// this.bp = bp;
		// this.bp2 = this.bp;
		ThredTestSingleton.bp=bp;
		//ThredTestSingleton.bp2 = this.bp;
		// System.out.println(this.bp);
		
		System.out.println(ThredTestSingleton.bp);
	}

	/** 
	 * main:(这里用一句话描述这个方法的作用). <br/> 
	 * 
	 * @author xiongfei
	 * @param args
	 */
	public static void main(String[] args) {

		Thread thread1 = new Thread(new Thread1());
		Thread thread2 = new Thread(new Thread2());

		thread1.start();
		thread2.start();

		// System.out.println(new ThredTestSingleton().bp1 == new
		// ThredTestSingleton().bp2);
		/*while (true) {
			// System.out.println(singleton.bp1 == singleton.bp2);
			System.out.println(bp1 == bp2);
		}*/
	}

}

class Thread1 implements Runnable {

	ThredTestSingleton singleton = ThredTestSingleton.singleton;
	@Override
	public void run() {
		while (true) {
			// ThredTestSingleton singleton = new ThredTestSingleton();
			singleton.init1(new BeanParam("1"));
		}

	}
}

class Thread2 implements Runnable {

	ThredTestSingleton singleton = ThredTestSingleton.singleton;
	@Override
	public void run() {
		while (true) {
			// ThredTestSingleton singleton = new ThredTestSingleton();
			singleton.init2(new BeanParam("1"));
		}
	}
}
