/** 
 * Project Name:ThreadTest 
 * File Name:BeanParam.java 
 * Package Name:test.singleton 
 * Copyright (c) 2017, randeexiong@163.com All Rights Reserved. 
 */

package test.singleton;

/**
 * <p>Title:BeanParam</p>
 * <p>Description:TODO</p>
 * @author	xiongfei
 * @date	2017年6月15日 下午2:26:57
 * @version 1.0
 */

public class BeanParam {
	/**
	 * name:TODO
	 */
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BeanParam(String name) {
		super();
		this.name = name;
	}

}
