package test;

public class BatchBean {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BatchBean(String name) {
		super();
		this.name = name;
	}

	public BatchBean() {
		super();
	}

	@Override
	public String toString() {
		return "[name=" + name + "]";
	}

}
